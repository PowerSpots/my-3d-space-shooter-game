﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{

    // 控制飞船的控制杆
    public VirtualJoystick steering;

    // 射击之间的延迟
    public float fireRate = 0.5f;

    // 当前开火的发射脚本
    private ShipWeapons currentWeapons;

    // 如果为真，飞船开火
    private bool isFiring = false;

    // 由ShipWeapons脚本调用改变当前武器
    public void SetWeapons(ShipWeapons weapons)
    {
        this.currentWeapons = weapons;
    }

    // 由ShipWeapons脚本调用重设当前武器变量
    public void RemoveWeapons(ShipWeapons weapons)
    {
        // 如果当前武器变量是要移除的武器参数，设为空
        if (this.currentWeapons == weapons)
        {
            this.currentWeapons = null;
        }
    }

    // 当发射按钮被按下时调用
    public void StartFiring()
    {
        // 启动开火协程
        StartCoroutine(FireWeapons());
    }


    IEnumerator FireWeapons()
    {
        
        // 标记开火条件为真
        isFiring = true;
        
        // 开火条件为真时循环
        while (isFiring)
        {
            // 开火间隔
            yield return new WaitForSeconds(fireRate);

            // 如果存在武器脚本，通知它开火
            if (this.currentWeapons != null)
            {
                currentWeapons.Fire();
            }
            
        }
    }

    // 停止触摸发射按钮时调用
    public void StopFiring()
    {
        // 开火条件为假时不进入开火协程中的循环
        isFiring = false;
    }
}
