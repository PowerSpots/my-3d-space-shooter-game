﻿using UnityEngine;
using System.Collections.Generic;


public class BulletsPool : Singleton<BulletsPool>
{
    //子弹perfabs
    public GameObject bulletObj;
    //子弹池初始大小
    public int pooledAmount = 40;
    //是否锁定子弹池大小
    public bool lockPoolSize = false;

    public GameObject poolParent;

    //子弹池链表，这里的集合可看作为对象池
    private List<GameObject> pooledObjects;
    //当前指向链表位置索引
    private int currentIndex = 0;

    void Start()
    {
        //初始化链表
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < pooledAmount; ++i)
        {
            //创建子弹对象
            GameObject obj = Instantiate(bulletObj);
            obj.transform.SetParent(poolParent.transform);
            //设置子弹无效
            obj.SetActive(false);
            //把子弹添加到链表（对象池）中
            pooledObjects.Add(obj);                     
        }
    }

    //获取对象池中可以使用的子弹
    public GameObject GetPooledObject()                 
    {
        for (int i = 0; i < pooledObjects.Count; ++i)
        {
            //简单的优化：每一次遍历都是从上一次被使用的子弹的下一个，而不是每次遍历从0开始
            //例：上一次获取了第4个子弹，currentIndex就为5，这里从索引5开始遍历，这是一种贪心算法
            int temI = (currentIndex + i) % pooledObjects.Count;
            //判断该子弹是否在场景中激活
            if (!pooledObjects[temI].activeInHierarchy) 
            {
                currentIndex = (temI + 1) % pooledObjects.Count;
                //找到没有被激活的子弹并返回
                return pooledObjects[temI];             
            }
        }

        //如果遍历完一遍子弹库发现没有可用的子弹，执行下面代码
        //如果没有锁定对象池大小，创建子弹并添加到对象池中
        if (!lockPoolSize)                               
        {
            GameObject obj = Instantiate(bulletObj);
            //禁用子弹
            obj.SetActive(false);
            pooledObjects.Add(obj);
            return obj;
        }

        //如果遍历完没有可用的子弹，而且锁定了对象池大小，返回空
        return null;
    }

    public void MyDisable(GameObject disableBullet)
    {
        //将传进来的对象隐藏（处于非激活状态）
        disableBullet.SetActive(false);
    }
}