﻿using UnityEngine;
using System.Collections;

public class ShipSteering : MonoBehaviour
{

    // 飞船旋转速率
    public float turnRate = 4.0f;

    // 飞船转回水平方向的作用力
    public float levelDamping = 2.0f;

    void Update()
    {
        // 通过将操纵杆的方向乘以旋转速度获得旋转方向，并将其限制在半圆的90％来生成新的平滑旋转

        // 获得用户输入的增量
        var steeringInput = InputManager.instance.steering.delta;

        // 创建旋转向量
        var rotation = new Vector2();

        rotation.y = steeringInput.x;
        rotation.x = steeringInput.y;

        // 乘以转向速率获得飞船旋转幅度
        rotation *= turnRate;

        // 将水平方向上旋转的弧度限制在半圆的90％
        rotation.x = Mathf.Clamp(rotation.x, -Mathf.PI * 0.9f, Mathf.PI * 0.9f);

        // 将弧度转换为旋转四元数
        var newOrientation = Quaternion.Euler(rotation);

        // 与目前的旋转方向复合
        transform.rotation *= newOrientation;

        // 尝试减少翻滚

        // 假设没有围绕Z轴翻滚，获得假设没有翻滚的欧拉角方向，并转换为四元数
        var levelAngles = transform.eulerAngles;
        levelAngles.z = 0.0f;
        var levelOrientation = Quaternion.Euler(levelAngles);

        // 将当前方向与假设方向值复合，并平滑翻滚回水平方向
        transform.rotation = Quaternion.Slerp(transform.rotation,
            levelOrientation, levelDamping * Time.deltaTime);

    }
}