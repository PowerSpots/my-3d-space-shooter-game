﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipTarget : MonoBehaviour {

    // 用于目标十字准星的精灵图片
    public Sprite targetImage;

    void Start ()
    {
        // 注册一个跟踪此对象的新指示器，使用黄色和自定义的精灵图片
        IndicatorManager.instance.AddIndicator(gameObject,Color.yellow, targetImage);
    } 
}
