﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour {


    void OnEnable()
    {
        //脚本可用的时候，重置子弹的位置
        //如果不加这句代码，从对象池中取出的子弹就会从上一次消失的位置开始运动。而不是你设定的子弹生成位置
        transform.position = Vector3.zero;
        //开启协程方法
        StartCoroutine(DelayDisable());
    }

    // 射击速度
    public float speed = 110.0f;

    // 在一段时间后禁用对象
    public float life = 2.0f;

    IEnumerator DelayDisable()
    {
        //等待三秒
        yield return new WaitForSeconds(life);
        //调用单例中向对象池里面存对象的方法
        BulletsPool.instance.MyDisable(gameObject);
    }

    void Update()
    {
        // 以恒定速度向前移动
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
}
