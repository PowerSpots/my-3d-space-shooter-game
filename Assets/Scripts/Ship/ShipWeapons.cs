﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipWeapons : MonoBehaviour {

    // 每次射击要使用的弹药预制件
    public GameObject shotPrefab;

    // 开火位置的对象数组
    public Transform[] firePoints;

    // 开火位置数组中将要开火的索引
    private int firePointIndex;

    public void Awake()
    {
        // 当武器对象启动时，将输入管理器当前的武器对象注册为当前脚本实例
        InputManager.instance.SetWeapons(this);
    }

    public void OnDestroy()
    {
        // 只有游戏进行时才能取消注册武器对象
        if (Application.isPlaying == true)
        {
            InputManager.instance.RemoveWeapons(this);
        }
    }

    // 由InputManager调用
    public void Fire() {
        // 如果没有开火点，返回
        if (firePoints.Length == 0)               //可以发射子弹时间
            return;

        // 确定从哪个位置点开始射击
        var firePointToUse = firePoints[firePointIndex];


        // 用开火点的位置和旋转创建新的射击
        GameObject bullet = BulletsPool.instance.GetPooledObject();
        //不为空时执行
        if (bullet != null)                  
        {
            //激活子弹并初始化子弹的位置
            bullet.SetActive(true);         
            bullet.transform.position = firePointToUse.transform.position;
            bullet.transform.rotation = firePointToUse.transform.rotation;

            // 如果开火点有音源，播放声效
            var fireSound = firePointToUse.GetComponent<AudioSource>();
            if (fireSound)
            {
                fireSound.Play();
            }
        }
        else
        {
            Debug.Log("Run out of Amour!");
        }

        // 移到下一个开火点
        firePointIndex++;

        // 如果我们已经用过位置数组中的最后一个开火点，回到数组最开始的开火点
        if (firePointIndex >= firePoints.Length)
            firePointIndex = 0;
    }
} 
