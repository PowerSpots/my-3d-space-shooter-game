﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour {

    // 生成区域的半径
    public float radius = 250.0f;

    // 要生成的小行星预制件
    public Rigidbody asteroidPrefab;

    // 生成每个小行星的间隔时间和方差时间
    public float spawnRate = 5.0f;
    public float variance = 1.0f;

    // 小行星的目标
    public Transform target;

    // 如果为false，则停止生成
    public bool spawnAsteroids = false;

    void Start()
    {
        // 立即开始创建小行星的协程
        StartCoroutine(CreateAsteroids());
    }

    IEnumerator CreateAsteroids()
    {
        while (true)
        {

            // 计算下一个小行星什么时候出现
            float nextSpawnTime = spawnRate + Random.Range(-variance, variance);

            // 等待
            yield return new WaitForSeconds(nextSpawnTime);

            // 直到物理更新完成
            yield return new WaitForFixedUpdate();

            // 创建小行星
            CreateNewAsteroid();
        }

    }

    void CreateNewAsteroid()
    {

        // 检测是否要生成小行星，如果生成条件为false，退出循环
        if (spawnAsteroids == false)
        {
            return;
        }

        // 随机选择球体表面上的一个点
        var asteroidPosition = Random.onUnitSphere * radius;

        // 按小行星的比例缩放比例
        asteroidPosition.Scale(transform.lossyScale);

        // 根据小行星生成点的位置调整生成位置
        asteroidPosition += transform.position;

        // 实例化一个小行星
        var newAsteroid = Instantiate(asteroidPrefab);

        // 放在生成点
        newAsteroid.transform.position = asteroidPosition;

        // 指向目标
        newAsteroid.transform.LookAt(target);
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        // 使用生成点当前的位置和比例绘制
        Gizmos.matrix = transform.localToWorldMatrix;

        // 绘制一个代表生成区域的球体
        Gizmos.DrawWireSphere(Vector3.zero, radius);
    }

    public void DestroyAllAsteroids()
    {
        // 移除游戏中的所有小行星
        foreach (var asteroid in FindObjectsOfType<Asteroid>())
        {
            Destroy(asteroid.gameObject);
        }
    }
}
