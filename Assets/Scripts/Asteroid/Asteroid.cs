﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{

    // 小行星速度的
    public float speed = 10.0f;

    void Start()
    {
        // 设置刚体组件的速度
        GetComponent<Rigidbody>().velocity = transform.forward * speed;

        // 为小行星创建一个红色指示器
        var indicator = IndicatorManager.instance.AddIndicator(gameObject, Color.red);

        indicator.showDistanceTo = GameManager.instance.currentSpaceStation.transform;
    }
}
