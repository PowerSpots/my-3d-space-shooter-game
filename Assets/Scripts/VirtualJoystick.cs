﻿using UnityEngine;
using System.Collections;
// 获得事件接口
using UnityEngine.EventSystems;
using UnityEngine.UI;

// 拖拽行为的三个关键接口
public class VirtualJoystick : MonoBehaviour, IBeginDragHandler,
    IDragHandler, IEndDragHandler
{
    // 被拖拽的拇指精灵图片
    public RectTransform thumb;

    // 没有拖拽时拇指标记和控制杆的初始位置
    private Vector2 originalPosition;
    private Vector2 originalThumbPosition;

    // 拇指标记被拖离初始位置的距离
    public Vector2 delta;

    void Start()
    {
        // 记录开始时控制杆和拇指标记的初始位置
        originalPosition = this.GetComponent<RectTransform>().localPosition;
        originalThumbPosition = thumb.localPosition;

        // 禁用拇指标记
        thumb.gameObject.SetActive(false);

        // 重设增量
        delta = Vector2.zero;
    }

    // 拖拽开始时调用的方法
    public void OnBeginDrag(PointerEventData eventData)
    {

        // 显示拇指标记
        thumb.gameObject.SetActive(true);

        // 计算并保存拖拽开始时控制杆的世界坐标
        Vector3 worldPoint = new Vector3();
        RectTransformUtility.ScreenPointToWorldPointInRectangle(
            this.transform as RectTransform,
            eventData.position,
            eventData.enterEventCamera,
            out worldPoint);


        // 将控制杆放在世界坐标上
        this.GetComponent<RectTransform>().position = worldPoint;

        // 确保拇指标记相对于控制杆在初始坐标上
        thumb.localPosition = originalThumbPosition;
    }

    // 拖拽进行时调用
    public void OnDrag(PointerEventData eventData)
    {

        // 计算拖拽时的世界坐标位置
        Vector3 worldPoint = new Vector3();
        RectTransformUtility.ScreenPointToWorldPointInRectangle(
            this.transform as RectTransform,
            eventData.position,
            eventData.enterEventCamera,
            out worldPoint);

        // 将拇指标记放在拖拽时的位置
        thumb.position = worldPoint;

        // 测量从操作板中心到拇指的距离，并存储在增量中
        var size = GetComponent<RectTransform>().rect.size;

        delta = thumb.localPosition;

        delta.x /= size.x / 2.0f;
        delta.y /= size.y / 2.0f;

        delta.x = Mathf.Clamp(delta.x, -1.0f, 1.0f);
        delta.y = Mathf.Clamp(delta.y, -1.0f, 1.0f);


    }

    // 拖拽结束时调用
    public void OnEndDrag(PointerEventData eventData)
    {
        // 重设控制杆位置
        this.GetComponent<RectTransform>().localPosition = originalPosition;

        // 增量设回0
        delta = Vector2.zero;

        // 隐藏拇指标记
        thumb.gameObject.SetActive(false);
    }
}