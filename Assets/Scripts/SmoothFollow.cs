﻿using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    // 要跟随的目标对象位置
    public Transform target;

    // 相机高于跟随目标的高度
    public float height = 5.0f;

    // 相机与目标的距离，不计高度
    public float distance = 10.0f;

    // 旋转角度和高度的跟随延迟
    public float rotationDamping;
    public float heightDamping;

    void LateUpdate()
    {
        // 如果没有目标，退出
        if (!target)
            return;

        // 计算当前的旋转角度和高度
        var wantedRotationAngle = target.eulerAngles.y;
        var wantedHeight = target.position.y + height;

        // 记录相机当前的位置和拍摄方向
        var currentRotationAngle = transform.eulerAngles.y;
        var currentHeight = transform.position.y;

        // 延缓围绕y轴的旋转
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle,
            wantedRotationAngle, rotationDamping * Time.deltaTime);

        // 延缓高度变化
        currentHeight = Mathf.Lerp(currentHeight,
            wantedHeight, heightDamping * Time.deltaTime);

        // 将角度转换为旋转
        var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

        // 设置相机在x-z平面上的位置：在目标后面一段距离
        transform.position = target.position;
        transform.position -= currentRotation * Vector3.forward * distance;

        // 设置相机高度
        transform.position = new Vector3(transform.position.x,
            currentHeight, transform.position.z);

        // 设置相机的方向
        transform.rotation = Quaternion.Lerp(transform.rotation,
            target.rotation, rotationDamping * Time.deltaTime);

    }
}