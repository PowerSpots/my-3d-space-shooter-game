﻿using UnityEngine;
using UnityEngine.UI;

public class IndicatorManager : Singleton<IndicatorManager>
{
    // 所有指示器的父对象
    public RectTransform labelContainer;

    // 指示器预制件
    public Indicator indicatorPrefab;

    public Indicator AddIndicator(GameObject target,Color color, Sprite sprite = null)
    {
        // 实例化预制
        var newIndicator = Instantiate(indicatorPrefab);

        // 追踪目标
        newIndicator.target = target.transform;

        // 更新颜色
        newIndicator.color = color;

        // 如果被设置一个精灵图片，更新指示器的精灵图片
        if (sprite != null)
        {
            newIndicator.GetComponent<Image>().sprite = sprite;
        }
        // 设为容器的子对象
        newIndicator.transform.SetParent(labelContainer, false);

        return newIndicator;
    }
} 