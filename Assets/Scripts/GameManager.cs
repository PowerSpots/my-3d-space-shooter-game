﻿using UnityEngine;
using System.Collections;

// BEGIN 3d_gamemanager
public class GameManager : Singleton<GameManager>
{

    // 飞船预制件、飞船生成点和当前飞船对象
    public GameObject shipPrefab;
    public Transform shipStartPosition;
    public GameObject currentShip { get; private set; }

    // 太空站预制件、生成点和当前太空站对象
    public GameObject spaceStationPrefab;
    public Transform spaceStationStartPosition;
    public GameObject currentSpaceStation { get; private set; }

    // 相机跟随脚本
    public SmoothFollow cameraFollow;

    // 游戏边界
    public Boundary boundary;

    // 游戏UI容器对象
    public GameObject inGameUI;
    public GameObject pausedUI;
    public GameObject gameOverUI;
    public GameObject mainMenuUI;

    // 当接近边界时出现的警告界面
    public GameObject warningUI;

    // 指示游戏是否在进行的状态
    public bool gameIsPlaying { get; private set; }

    // 小行星生成器
    public AsteroidSpawner asteroidSpawner;

    // 跟踪游戏的暂停状态
    public bool paused;

    // 开始主菜单
    void Start()
    {
        ShowMainMenu();
    }

    // 显示当前菜单容器并隐藏其他菜单
    void ShowUI(GameObject newUI)
    {

        // UI容器数组
        GameObject[] allUI = { inGameUI, pausedUI, gameOverUI, mainMenuUI };

        // 隐藏所有菜单
        foreach (GameObject UIToHide in allUI)
        {
            UIToHide.SetActive(false);
        }

        // 激活要显示的菜单容器
        newUI.SetActive(true);
    }

    public void ShowMainMenu()
    {
        // 显示主菜单容器
        ShowUI(mainMenuUI);

        // 游戏开始时，我们没有在进行游戏
        gameIsPlaying = false;

        // 不要生成小行星
        asteroidSpawner.spawnAsteroids = false;
    }
    // 开始游戏按钮被按下时调用
    public void StartGame()
    {
        // 显示游戏内UI
        ShowUI(inGameUI);

        // 正在游戏状态
        gameIsPlaying = true;

        // 如果有飞船已经在游戏中，摧毁
        if (currentShip != null)
        {
            Destroy(currentShip);
        }

        // 如果有太空站已经在游戏中，摧毁
        if (currentSpaceStation != null)
        {
            Destroy(currentSpaceStation);
        }

        // 实例化飞船，放在生成点的位置和旋转
        currentShip = Instantiate(shipPrefab);
        currentShip.transform.position = shipStartPosition.position;
        currentShip.transform.rotation = shipStartPosition.rotation;

        // 实例化太空站，放在生成点的位置和旋转
        currentSpaceStation = Instantiate(spaceStationPrefab);
        currentSpaceStation.transform.position = spaceStationStartPosition.position;
        currentSpaceStation.transform.rotation = spaceStationStartPosition.rotation;

        // 相机开始跟随新飞船
        cameraFollow.target = currentShip.transform;

        // 开始生成小行星
        asteroidSpawner.spawnAsteroids = true;

        // 小行星带目标设为太空站
        asteroidSpawner.target = currentSpaceStation.transform;

    }

    // 当能结束游戏的对象被摧毁时调用
    public void GameOver()
    {
        // 现实游戏结束界面
        ShowUI(gameOverUI);

        // 设为未在游戏状态
        gameIsPlaying = false;

        // 摧毁游戏中的飞船和太空站
        if (currentShip != null)
            Destroy(currentShip);
        if (currentSpaceStation != null)
            Destroy(currentSpaceStation);

        // 停止显示警告界面
        warningUI.SetActive(false);


        // 停止生产小行星
        asteroidSpawner.spawnAsteroids = false;

        // 移除游戏中所有残留的小行星
        asteroidSpawner.DestroyAllAsteroids();
    }

    // 按下暂停或恢复按钮时调用
    public void SetPaused(bool paused)
    {

        // 切换暂停和游戏中界面
        inGameUI.SetActive(!paused);
        pausedUI.SetActive(paused);

        // 如果暂停状态，时间停滞，否则恢复游戏
        if (paused)
        {
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }

    // 管理边界数据
    public void Update()
    {

        // 没有飞船，返回
        if (currentShip == null)
            return;

        // 如果飞船在边界的毁灭半径外，则游戏结束
        // 如果在边界的毁灭半径内，但在警告半径外，则显示警告界面
        // 如果它在两者之内，不显示警告用户界面

        float distance =
            (currentShip.transform.position
                - boundary.transform.position).magnitude;

        if (distance > boundary.destroyRadius)
        {
            // 超过毁灭半径，结束游戏
            GameOver();
        }
        else if (distance > boundary.warningRadius)
        {
            // 显示警告
            warningUI.SetActive(true);
        }
        else
        {
            // 不显示
            warningUI.SetActive(false);
        }
    }
}