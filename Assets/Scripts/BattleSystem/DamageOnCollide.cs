﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnCollide : MonoBehaviour {

    // 对击中的任何物体造成的伤害
    public int damage = 1;

    // 击中其他物体对自己造成的伤害
    public int damageToSelf = 5;

    void HitObject(GameObject theObject)
    {
        // 如果击中的对象有DamageTaking脚本，对它造成伤害
        var theirDamage = theObject.GetComponentInParent<DamageTaking>();
        if (theirDamage)
        {
            theirDamage.TakeDamage(damage);
        }

        // 如果自己有DamageTaking脚本，对自己造成伤害
        var ourDamage = this.GetComponentInParent<DamageTaking>();
        if (ourDamage)
        {
            ourDamage.TakeDamage(damageToSelf);
        }
    }

    // 检测物体是否进入触发器区域
    void OnTriggerEnter(Collider collider)
    {
        HitObject(collider.gameObject);
    }

    // 检测物体是否与自己发生碰撞
    void OnCollisionEnter(Collision collision)
    {
        HitObject(collision.gameObject);
    }
}
