﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTaking : MonoBehaviour {

    // 对象具有的生命值
    public int hitPoints = 10;

    // 如果对象被摧毁，在当前位置实例化一个预制件（特效）
    public GameObject destructionPrefab;

    // 如果这个对象被销毁，我们是否结束游戏
    public bool gameOverOnDestroyed = false;
    private GameObject expolsions;

    // 由其他对象调用来造成伤害
    public void TakeDamage(int amount)
    {
        Debug.Log(gameObject.name + " damaged!");

        // 生命值减去伤害点
        hitPoints -= amount;

        // 检查生命值是否小于0
        if (hitPoints <= 0)
        {
            Debug.Log(gameObject.name + " destroyed!");

            // 死亡后移除对象
            Destroy(gameObject);

            // 是否有预制件要在对象死亡后实例化
            if (destructionPrefab != null)
            {

                // 用对象当前位置和角度实例化预制件
                expolsions = Instantiate(destructionPrefab,
                    transform.position, transform.rotation);
                Destroy(expolsions, 1.5f);

            }

            // 如果结束游戏的状态为真，调用游戏管理器中的方法
            if (gameOverOnDestroyed == true)
            {
                GameManager.instance.GameOver();
            }
        }

    }
}
