﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{

    // 要追踪目标的位置
    public Transform target;

    // 太空站的位置
    public Transform showDistanceTo;

    // 显示到目标距离的文本
    public Text distanceLabel;

    // 保证标签始终可读的额外参数
    public int margin = 50;

    // 图像色调属性
    public Color color
    {
        set
        {
            GetComponent<Image>().color = value;
        }
        get
        {
            return GetComponent<Image>().color;
        }
    }

    void Start()
    {
        // 隐藏文本，直到需要时调用
        distanceLabel.enabled = false;

        // 第一帧不渲染指示器图片，防止视觉毛刺
        GetComponent<Image>().enabled = false;

    }

    // 每帧更新指示器的位置
    void Update()
    {

        // 如果目标不存在，摧毁指示器
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        // 如果有需要计算距离的目标，计算并将其显示在文本中
        if (showDistanceTo != null)
        {

            // 激活显示文本
            distanceLabel.enabled = true;

            // 计算太空站到追踪目标的距离
            var distance = (int)Vector3.Magnitude(
                showDistanceTo.position - target.position);

            // 更新文本内容
            distanceLabel.text = distance.ToString() + "m";
        }
        else
        {
            // 隐藏文本
            distanceLabel.enabled = false;
        }
        //激活指示器
        GetComponent<Image>().enabled = true;

        // 将指示器正在跟踪的对象的3D坐标转换为视口空间
        var viewportPoint = Camera.main.WorldToViewportPoint(target.position);

        //如果追踪对象在我们身后
        if (viewportPoint.z < 0)
        {
            // x分量通过乘以无穷大来让指示器将始终位于屏幕的最左侧或最右侧
            viewportPoint.z = 0;
            viewportPoint = viewportPoint.normalized;
            viewportPoint.x *= -Mathf.Infinity;
        }

        // 将视口空间坐标转换为屏幕空间
        var screenPoint = Camera.main.ViewportToScreenPoint(viewportPoint);

        // 截取屏幕空间坐标，确保指示器永远不会出现在屏幕外，显示距离的文本标签始终可读
        screenPoint.x = Mathf.Clamp(screenPoint.x, margin, Screen.width - margin * 2);
        screenPoint.y = Mathf.Clamp(screenPoint.y, margin, Screen.height - margin * 2);


        // 将屏幕空间坐标转换为指示器容器的坐标空间，用于更新指示器的位置
        var localPosition = new Vector2();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            transform.parent.GetComponent<RectTransform>(),
            screenPoint,
            Camera.main,
            out localPosition);

        // 更新位置
        var rectTransform = GetComponent<RectTransform>();
        rectTransform.localPosition = localPosition;

    }
}